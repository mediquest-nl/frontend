# nl.mediquest.frontend

## Development Mode

### Run application:

```
lein clean
lein deps
lein npm install
lein sass4clj auto
shadow-cljs watch dev
```

#### Compile css:

```
lein sass4clj once
```

When you get an `file not found error` you have to update the npm dependencies:

```
lein npm install
```

#### Run application in Emacs:

```
M-x cider-jack-in-clojurescript
Select ClojureScript REPL type: shadow
Select shadow-cljs build (e.g. dev): app
```

ShadowCljs will automatically push cljs changes to the browser.

Wait a bit, then browse to [http://localhost:8280](http://localhost:8280).

### Deploying CSS

```
lein sass4clj once
gsutil cp resources/public/style.css gs://frontend-assets-bucket/css/[VERSION]/style.css
```
