(ns nl.mediquest.frontend.views
  (:require
   [re-frame.core :as re-frame]
   [nl.mediquest.frontend.subs :as subs]
   [nl.mediquest.frontend.component.dropdown.core :as component.dropdown]))

(def dropdown-items
  [{:content "No User" :value nil}
   {:content "User1" :value 1}
   {:content "User2" :value 2}
   {:content "User3" :value 3}
   {:content "User4" :value 4}
   {:content "User5" :value 5}
   {:content "User6" :value 6}
   {:content "User7" :value 7}
   {:content "User8" :value 8}
   {:content "User9" :value 9}])

(defn dropdown-example [title props]
  [component.dropdown/init
   (merge
    {:dropdown-name title
     :title title
     :items dropdown-items}
    props)])

(defn dropdowns []
  [:div.has-padding-4
   [:div.title.is-size-4 "Dropdowns"]
   [:div.columns
    [:div.column
     [:div.title.is-size-6 "Default"]
     [dropdown-example "example1"]]
    [:div.column
     [:div.title.is-size-6 "With search"]
     [dropdown-example "example2" {:show-search? true}]]
    [:div.column
     [:div.title.is-size-6 "Multi select (Check Boxes)"]
     [dropdown-example "example3" {:multi-select? true}]]
    [:div.column
     [:div.title.is-size-6 "Multi select + Search"]
     [dropdown-example "example4" {:show-search? true
                                   :multi-select? true}]]]])

(defn main-panel []
  [:div.has-padding-4
   [:div.title "Mediquest Frontend Components"]
   [dropdowns]])
